#!/usr/bin/env python
import extractor_util as util
import sys

parser = util.RowParser([
          ('ps_id', 'text'),
          ('omim_id', 'text'),
          ('hpo_ids', 'text[]')])


if __name__ == '__main__':
  ps_to_hpo = {}
  
  for line in sys.stdin:
    row = parser.parse_tsv_row(line)
    if row.ps_id in ps_to_hpo:
      current_hpo_ids = ps_to_hpo[row.ps_id]
      intsect = current_hpo_ids.intersection(row.hpo_ids)
      # TODO maybe in the future do intersection over some intelligent
      # canonicalization before intersecting ...
      ps_to_hpo[row.ps_id] = intsect
    else:
      ps_to_hpo[row.ps_id] = set(row.hpo_ids)
  
  for ps_id in ps_to_hpo:
    hpo_ids = ps_to_hpo[ps_id]
    for hpo_id in hpo_ids:
      print "%s\t%s" % (ps_id, hpo_id)  
