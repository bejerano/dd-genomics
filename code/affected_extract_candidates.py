#!/usr/bin/env python
import collections
import extractor_util as util
import data_util as dutil
import random
import re
import os
import sys
import string
import config
import dep_util as deps

CACHE = dict()  # Cache results of disk I/O


# This defines the Row object that we read in to the extractor
parser = util.RowParser([
          ('doc_id', 'text'),
          ('section_id', 'text'),
          ('sent_id', 'int'),
          ('words', 'text[]'),
          ('dep_paths', 'text[]'),
          ('dep_parents', 'int[]'),
          ('lemmas', 'text[]'),
          ('poses', 'text[]'),
          ('ners', 'text[]')])


# This defines the output Mention object
Mention = collections.namedtuple('Mention', [
            'dd_id',
            'doc_id',
            'section_id',
            'sent_id',
            'wordidxs',
            'mention_id',
            'mapping_type',
            'mention_supertype',
            'mention_subtype',
            'affected_name',
            'words',
            'is_correct'])

### CANDIDATE EXTRACTION ###
HF = config.AFFECTED['HF']
SR = config.AFFECTED['SR']

### DISTANT SUPERVISION ###
def create_supervised_mention(row, i):
  word = row.words[i]
  mid = '%s_%s_%s_%s' % (row.doc_id, row.section_id, row.sent_id, i)
  m = Mention(None, row.doc_id, row.section_id, row.sent_id, [i], mid, \
              None, None, None, word, [word], 't')
  return m

def extract_candidate_mentions(row):
  mentions = []
  for i in xrange(len(row.words)):
    word = row.words[i].lower()
    lemma = row.lemmas[i].lower()
    if word in HF['names'] or lemma in HF['names']:
      m = create_supervised_mention(row, i)
      if m:
        mentions.append(m)
  return mentions

if __name__ == '__main__':
  for line in sys.stdin:
    row = parser.parse_tsv_row(line)

    # Skip row if sentence doesn't contain a verb, contains URL, etc.
    if util.skip_row(row):
      continue

    # Find candidate mentions & supervise
    mentions = extract_candidate_mentions(row)

    # print output
    for mention in mentions:
      util.print_tsv_output(mention)
