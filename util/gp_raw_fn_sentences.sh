#!/bin/bash -e
set -beEu -o pipefail


if [ $# -eq 1 ]
then
  version_string="AND version = $1"
else
  version_string=""
fi

GP_CUTOFF=`cat ../results_log/gp_cutoff`

cd ..
source env_local.sh
deepdive sql """
COPY (
SELECT DISTINCT
  l.labeler CAUSATION_FALSE_NEGATIVES,
  gc.relation_id,
  si.doc_id,
  si.section_id,
  si.sent_id,
  gc.supertype,
  gc.subtype,
  gc.gene_name,
  gc.gene_wordidxs,
  gc.pheno_wordidxs,
  (string_to_array(si.words, '|^|'))[(gc.pheno_wordidxs)[1] + 1] first_pheno,
  array_to_string(string_to_array(si.words, '|^|'), ' ') words,
  array_to_string(string_to_array(si.lemmas, '|^|'), ' ') lemmas
FROM
  genepheno_causation_no_charite gc 
  RIGHT JOIN (select distinct * from
      ((select * from genepheno_causation_labels)
      union (select * from genepheno_causation_precision_labels)) a) l
    ON (l.relation_id = gc.relation_id)
  JOIN sentences_input si
    ON (si.doc_id = gc.doc_id AND si.section_id = gc.section_id AND si.sent_id = gc.sent_id)
WHERE
  gc.is_correct = 'f'
  AND l.is_correct = 't'
  $version_string)
TO STDOUT;
"""
