#! /bin/bash -e

deepdive sql """
COPY (SELECT hpo_id, ensembl_id FROM genepheno_causation_joined_phenos_raw ORDER BY hpo_id, ensembl_id) TO STDOUT
""" | awk '{OFS="\t"; print $1, $2, "DEEPDIVE:"substr($2,5)}'
