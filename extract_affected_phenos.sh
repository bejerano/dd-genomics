#! /bin/bash

doc=$1

deepdive sql """
COPY (
select
  a.section_id,
  a.sent_id,
  a.affected_count,
  b.pheno_count,
  ARRAY_TO_STRING(STRING_TO_ARRAY(si.words, '|^|'), ' ') words
FROM
  (select
    si.section_id,
    si.sent_id, 
    ARRAY_AGG(a.affected_name) affected, 
    count(distinct mention_id) affected_count
  from 
    affected_mentions a 
    right join sentences_input si 
      on (a.doc_id = si.doc_id and a.section_id = si.section_id and a.sent_id = si.sent_id) 
  where 
    si.doc_id = '${doc}' 
    and si.section_id like 'Body.%' 
  group by 
    si.section_id,
    si.sent_id) a
  JOIN
  (select
    si.section_id,
    si.sent_id, 
    ARRAY_AGG(a.entity) phenos,
    count(distinct mention_id) pheno_count
  from 
    pheno_mentions a
    right join sentences_input si 
      on (a.doc_id = si.doc_id and a.section_id = si.section_id and a.sent_id = si.sent_id) 
  where 
    si.doc_id = '${doc}' 
    and si.section_id like 'Body.%' 
  group by 
    si.section_id,
    si.sent_id) b
  ON
    (a.section_id = b.section_id and a.sent_id = b.sent_id)
  JOIN sentences_input si
    ON (si.doc_id = '${doc}' and si.section_id = b.section_id and si.sent_id = b.sent_id)
order by
  a.section_id, a.sent_id
) TO STDOUT
"""
