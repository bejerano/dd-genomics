copy (
SELECT * FROM (
select DISTINCT ON (g.canonical_name, p.pheno_entity)
  si.doc_id 
  , si.section_id
  , si.sent_id
  , g.canonical_name as gene_name
  , p.pheno_entity as pheno_id
  , (STRING_TO_ARRAY(pn.names, '|^|'))[1] as pheno_name
  , STRING_TO_ARRAY(p.gene_wordidxs, '|^|') as gene_wordidxs
  , STRING_TO_ARRAY(p.pheno_wordidxs, '|^|') as pheno_wordidxs
  , string_to_array(si.words, '|^|') as words
  , p.relation_id as row_id
  , gc.supertype
  , gc.subtype
from
  (SELECT hpo_id, ensembl_id, unnest(relation_ids) relation_id, unnest(supertypes) supertype, unnest(subtypes) subtype FROM genepheno_causation_inferred_raw i WHERE (hpo_id, ensembl_id) NOT IN (SELECT hpo_id, ensembl_id FROM charite_canon cc) AND (hpo_id, ensembl_id) NOT IN (SELECT omim_id, ensembl_id FROM charite_disease_canon cc) ORDER BY random()) gc
  JOIN genepheno_pairs p on (gc.relation_id = p.relation_id)
  JOIN sentences_input si on (p.doc_id = si.doc_id and p.section_id = si.section_id and p.sent_id = si.sent_id)
  JOIN genes g on (gc.ensembl_id = g.ensembl_id)
  JOIN pheno_names pn on (p.pheno_entity = pn.id)
) a
ORDER BY random()
LIMIT 1000
)
to stdout with csv header;
