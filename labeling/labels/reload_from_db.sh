#! /bin/zsh -x

psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (select distinct labeler from genepheno_causation_labels) TO STDOUT""" | while read labeler
do
  psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (SELECT DISTINCT * FROM genepheno_causation_labels WHERE labeler = '${labeler}' ORDER BY version, relation_id) to stdout;""" > genepheno_causation_${labeler}
done
psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (select distinct labeler from genepheno_association_labels) TO STDOUT""" | while read labeler
do
  psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (SELECT DISTINCT * FROM genepheno_association_labels WHERE labeler = '${labeler}' ORDER BY version, relation_id) to stdout;""" > genepheno_association_${labeler}
done
psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (select distinct labeler from genepheno_causation_precision_labels) TO STDOUT""" | while read labeler
do
  psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (SELECT DISTINCT * FROM genepheno_causation_precision_labels WHERE labeler = '${labeler}' ORDER BY version, relation_id) to stdout;""" > genepheno_causation_precision_${labeler}
done
psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (select distinct labeler from genepheno_multi_precision_labels) TO STDOUT""" | while read labeler
do
  psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (SELECT DISTINCT * FROM genepheno_multi_precision_labels WHERE labeler = '${labeler}' ORDER BY version, relation_id) to stdout;""" > genepheno_multi_precision_${labeler}
done
psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (select distinct labeler from genepheno_facts_precision_labels) TO STDOUT""" | while read labeler
do
  psql -p 6432 -h raiders7 -U jbirgmei genomics_labels -c """COPY (SELECT DISTINCT * FROM genepheno_facts_precision_labels WHERE labeler = '${labeler}' ORDER BY version, relation_id) to stdout;""" > genepheno_facts_precision_${labeler}
done
